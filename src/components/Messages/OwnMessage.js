import React from "react";
import {Button, Grid, Header, Message} from "semantic-ui-react";
import PropTypes from "prop-types";
import ForeignMessage from "./ForeignMessage";

class OwnMessage extends React.Component {

    render() {
        const message = this.props.message;
        const newText = message.text.split('\n').map((item, i) => {
            return <p key={i}>{item}</p>;
        });

        return (
            <Message>
                <Grid>
                    <Grid.Column width={3}>
                        <Button.Group>
                            <Button basic onClick={() => this.props.deleteMessage(message)}>Delete</Button>
                            <Button onClick={() => this.props.startEditing(message)}>Edit</Button>
                        </Button.Group>
                    </Grid.Column>
                    <Grid.Column width={10} floated={"right"}>
                        <Header as='h5'>
                            {"Me" + " at " + getTimeString(message.createdAt)}
                        </Header>
                        <p>{newText}</p>
                    </Grid.Column>

                </Grid>
            </Message>
        )
    }
}

OwnMessage.propTypes = {
    message: PropTypes.object.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    startEditing: PropTypes.func.isRequired
}

function getTimeString(date) {
    date = new Date(date);
    const hours = date.getHours();
    const minutes = date.getMinutes();

    return "" + Math.floor(hours / 10) + hours % 10 +
        ":" + Math.floor(minutes / 10) + minutes % 10;
}

export default OwnMessage