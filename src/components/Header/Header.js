import React from "react";
import {Segment} from 'semantic-ui-react'
import PropTypes from 'prop-types';

class Header extends React.Component {

    render() {
        return (
            <Segment.Group horizontal raised>
                <Segment>{this.props.chatName}</Segment>
                <Segment>{"Participants: " + this.props.usersCount}</Segment>
                <Segment>{"Messages: " + this.props.messagesCount}</Segment>
                <Segment>{"Last message on: " + new Date(this.props.lastDate).toDateString()}</Segment>
            </Segment.Group>
        )
    }
}

Header.propTypes = {
    chatName: PropTypes.string.isRequired,
    usersCount: PropTypes.number.isRequired,
    messagesCount: PropTypes.number.isRequired,
    lastDate: PropTypes.string.isRequired
}

export default Header;