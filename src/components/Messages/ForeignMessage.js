import React from "react";
import {Grid, GridColumn, Header, Icon, Image, Message} from "semantic-ui-react";
import PropTypes from 'prop-types';

class ForeignMessage extends React.Component {
    render() {
        const message = this.props.message;
        const newText = message.text.split('\n').map((item, i) => {
            return <p key={i}>{item}</p>;
        });
        return (
            <Message>
                <Grid>
                    <GridColumn width={15}>
                        <Header as='h5'>
                            <Image circular src={message.avatar}/>
                            {message.user + " at " + getTimeString(message.createdAt)}
                        </Header>
                        <p>{newText}</p>
                    </GridColumn>
                    <GridColumn width={1}>
                        {message.isLiked ?
                            <Icon onClick={() => this.props.likeMessage(message)}
                                  name='heart'
                                  color='red'
                                  size={"large"}
                            /> :
                            <Icon onClick={() => this.props.likeMessage(message)}
                                  name='heart outline'
                                  size={"large"}
                            />
                        }
                    </GridColumn>
                </Grid>
            </Message>
        )
    }
}

ForeignMessage.propTypes = {
    message: PropTypes.object.isRequired,
    likeMessage: PropTypes.func.isRequired
}


function getTimeString(date) {
    date = new Date(date);
    const hours = date.getHours();
    const minutes = date.getMinutes();

    return "" + Math.floor(hours / 10) + hours % 10 +
        ":" + Math.floor(minutes / 10) + minutes % 10;
}


export default ForeignMessage