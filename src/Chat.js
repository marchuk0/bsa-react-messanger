import React, {createRef} from 'react';
import {getMessages} from './services/messagesService';
import Header from "./components/Header/Header";
import Spinner from "./components/Spinner/Spinner";
import SendMessageForm from "./components/SendMessageForm/SendMessageForm";
import MessageList from './components/Messages/MessageList';
import {v4 as uuidv4} from 'uuid';
import {Sticky} from "semantic-ui-react";
import './Chat.css';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: null,
            user: {
                userId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
                avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
                user: "Ben"
            },
            newMessage: "",
            editedMessage: null
        };
        this.handleChangeMessage = this.handleChangeMessage.bind(this);
        this.handleSendMessage = this.handleSendMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.startEditing = this.startEditing.bind(this);
        this.cancelEditing = this.cancelEditing.bind(this);
        this.likeMessage = this.likeMessage.bind(this);
        this.saveEditing = this.saveEditing.bind(this);
    }

    contextRef = createRef();

    componentDidMount() {
        getMessages()
            .then(result => {
                result.sort(function (a, b) {
                    return a.createdAt - b.createdAt
                });
                this.setState({messages: result})
            });
    }

    handleChangeMessage(event) {
        this.setState({newMessage: event.target.value});
    }

    handleSendMessage(event) {
        event.preventDefault();
        if (this.state.newMessage === "") {
            return;
        }
        const message = {
            ...this.state.user,
            id: uuidv4(),
            text: this.state.newMessage,
            createdAt: new Date(),
            editedAt: ""
        }
        this.setState({
            newMessage: "",
            messages: [...this.state.messages, message]
        });
    }

    startEditing(message) {
        this.setState({
            editedMessage: message,
            newMessage: message.text
        });
    }

    cancelEditing() {
        this.setState({
            editedMessage: null,
            newMessage: ""
        });
    }

    saveEditing() {
        if (this.state.newMessage === "") {
            return;
        }
        const newMessage = this.state.editedMessage;
        newMessage.text = this.state.newMessage;
        newMessage.editedAt = new Date();
        this.setState({
            messages: this.state.messages,
            editedMessage: null,
            newMessage: ""
        });

    }

    deleteMessage(message) {
        if (message === this.state.editedMessage) {
            this.cancelEditing();
        }
        const newMessages = this.state.messages;
        const index = newMessages.indexOf(message);
        newMessages.splice(index, 1);
        this.setState({messages: this.state.messages});
    }

    likeMessage(message) {
        message.isLiked = !message.isLiked;
        this.setState({messages: this.state.messages});
    }


    render() {
        if (this.state.messages === null) {
            return (<Spinner/>);
        }
        const name = "MyChat";
        const usersCount = 12;
        const messagesCount = this.state.messages.length;
        const lastDate = messagesCount === 0 ? null : this.state.messages[messagesCount - 1].createdAt;
        // console.log(lastDate);
        return (
            <div ref={this.contextRef}>
                <Sticky context={this.contextRef}>
                    <div className="chat">
                        <Header chatName={name}
                                usersCount={usersCount}
                                messagesCount={messagesCount}
                                lastDate={lastDate}/>

                    </div>
                </Sticky>


                <MessageList messages={this.state.messages}
                             userId={this.state.user.userId}
                             startEditing={this.startEditing}
                             deleteMessage={this.deleteMessage}
                             likeMessage={this.likeMessage}
                />

                <SendMessageForm value={this.state.newMessage}
                                 onChange={this.handleChangeMessage}
                                 onSubmit={this.handleSendMessage}
                                 isEdited={this.state.editedMessage != null}
                                 onSave={this.saveEditing}
                                 onCancel={this.cancelEditing}
                />
            </div>
        );
    }
}

export default Chat;
