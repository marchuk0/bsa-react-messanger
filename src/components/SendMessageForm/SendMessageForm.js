import React, {createRef} from "react";
import {Button, Form, Segment, Sticky, TextArea} from "semantic-ui-react";
import './SendMessageForm.css'
import PropTypes from "prop-types";

class SendMessageForm extends React.Component {


    render() {
        const buttons = this.props.isEdited ?
            <div>
                <Button floated={"left"} onClick={this.props.onCancel}>Cancel</Button>
                <Button floated={"left"} onClick={this.props.onSave}>Save</Button>
            </div> :
            <div><Button floated={"left"} onClick={this.props.onSubmit}>Send Message</Button></div>;
        return (
            <Segment className="SendMessageForm">
                <Form>
                    <label>
                        <TextArea className="TextArea"
                                  value={this.props.value}
                                  onChange={this.props.onChange}
                                  placeholder="Write a message..."/>
                    </label>
                    {buttons}
                </Form>
            </Segment>
        );
    }

}

SendMessageForm.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired
}

export default SendMessageForm;