import {callApi} from './apiHelper';

export async function getMessages() {
    try {
        const apiResult = await callApi();
        return apiResult;
    } catch (error) {
        throw error;
    }
}