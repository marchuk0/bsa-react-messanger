import React from "react";
import ForeignMessage from "./ForeignMessage";
import {Divider, Segment} from "semantic-ui-react";
import OwnMessage from "./OwnMessage";
import PropTypes from 'prop-types';

class MessageList extends React.Component {
    render() {
        const messages = this.props.messages;
        const messagesList = [];
        for (var i = 0; i < messages.length; i++) {
            if (i === 0 || dateString(messages[i].createdAt) !== dateString(messages[i - 1].createdAt)) {
                messagesList.push(<Divider horizontal>{dateString(messages[i].createdAt)}</Divider>)
            }
            if (messages[i].userId === this.props.userId) {
                messagesList.push(<OwnMessage message={messages[i]}
                                              startEditing={this.props.startEditing}
                                              deleteMessage={this.props.deleteMessage}/>);
            } else {
                messagesList.push(<ForeignMessage message={messages[i]} likeMessage={this.props.likeMessage}/>);
            }

        }
        return (
            <Segment>
                {messagesList}
            </Segment>
        )
    }
}

function dateString(date) {
    return new Date(date).toDateString();
}

MessageList.propTypes = {
    messages: PropTypes.arrayOf(PropTypes.object).isRequired,
    startEditing: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired
}


export default MessageList;